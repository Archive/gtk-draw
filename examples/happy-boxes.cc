// This is -*- C++ -*-
// $Id$

/*
 * happy-boxes.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#include "drawingbuffer.h"
#include <gdk--.h>

class Gtk_Thing : public Gtk_DrawingBuffer {
public:
  void redraw();

  gint do_something(GdkEventMotion*) {
    static int i=0;

    // responding to *every* motion event bogs things down too much, even
    // on a relatively fast machine (or what passes for a fast machine in
    // mid 1998...)
    if (i == 10) { 
      redraw();
      refresh();
      i=0;
    }
    ++i;
    return 0;
  }

  gint quit(GdkEventButton* eb) {
    Gtk_Main::instance()->quit();
    return 0;
  }

};

void
Gtk_Thing::redraw()
{
  Gdk_Draw draw(buffer());
  Gdk_GC gc(buffer());

  const int i_steps = 5;
  const int j_steps = 5;
  for(gint i=0; i<i_steps; ++i)
    for(gint j=0; j<j_steps; ++j) {
      Gdk_Color c;
      c.set_random();
      draw.gc().set_foreground(c);
      draw.draw_rectangle(true,
		     (gint)(i*width())/(double)i_steps,
		     (gint)(j*height()/(double)j_steps),
		     (gint)((i+1)*width())/(double)i_steps,
		     (gint)((j+1)*height()/(double)j_steps));
    }
}

main(int argc, char* argv[])
{
  Gtk_Main gtkmain(&argc, &argv);

  Gtk_Window main_window;
  Gtk_Thing thing;

  main_window.add( &thing );
  thing.show();
  main_window.show();

  connect_to_method(thing.motion_notify_event, &thing,
		    &Gtk_Thing::do_something);

  connect_to_method(thing.button_press_event, &thing,
		    &Gtk_Thing::quit);

  gtkmain.run();

}




// $Id$

// This is -*- C++ -*-
// $Id$

/*
 * life.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

/* A gruesome, graceless hack --- I should be ashamed of myself... */

#include <time.h>
#include <math.h>

#include "timeout.h"
#include <gdk--.h>
#include "drawingbuffer.h"

class Life : public Gtk_DrawingBuffer, public Gtk_Idle {
public:
  Life(int r, int c) : Gtk_DrawingBuffer(), Gtk_Idle(),
    rows_(r), cols_(c),
    board1_(0), board2_(0), immediate_update(true) {

    connect_to_method(button_press_event, this, &button);

    live_.set("blue");
    dead_.set("white");
  }

  gint button(GdkEventButton* b) {
    if (b->button == 3) {
      stop();
      Gtk_Main::instance()->quit();
    } else {
      splat((gint)b->x,(gint)b->y);
    }
    return 0;
  }

  
  void init_board(int r, int c) {
    rows_ = r;
    cols_ = c;

    delete [] board1_;
    delete [] board2_;

    board1_ = new char[rows_*cols_];
    board2_ = new char[rows_*cols_];

    for(int i=0; i<rows_*cols_; ++i)
      board1_[i] = board2_[i] = 0;

    usingBoard1_ = true;
    generations_ = 0;
    time(&start_);
    NC = width()/c;
    NR = height()/r;
    offx_ = (width() - NC*c)/2;
    offy_ = (height() - NR*r)/2;
  }

  void randomize_board(int count) {
    if (count < 1)
      count = rows_*cols_/10;
    for(int i=0; i<count; ++i)
      cell(random() % rows_, random() % cols_) = 1;
  }

  void splat(int x, int y) {
    x = (x-offx_)/NC;
    y = (y-offy_)/NR;
    if (x < 0) x += cols_;
    if (y < 0) y += rows_;
    int m = (rows_+cols_)/2;
    for(int i=0; i<50; ++i) {
      double rad = 0.10*m*(random()/(double)RAND_MAX);
      double th = 2*3.14*(random()/(double)RAND_MAX);
      gint c = (gint)(x+rad*cos(th));
      gint r = (gint)(y+rad*sin(th));
      cell(r,c) = 1;
    }
  }

  char& cell(char* a, int r, int c) {
    return a[((rows_+r) % rows_)*cols_+((cols_+c) % cols_)];
  }

  char& cell(int r, int c) {
    return cell(usingBoard1_ ? board1_ : board2_, r, c);
  }

  void paint_cell(int r, int c, bool val) {
    draw_.gc().set_foreground(val ? live_ : dead_);
    draw_.draw_rectangle(true, offx_+NC*c, offy_+NR*r, NC, NR);
  }

  void paint_board() {
    draw_.gc().set_foreground(dead_);
    draw_.draw_rectangle(true, offx_,offy_,NC*cols_,NR*rows_);
    draw_.gc().set_foreground(live_);
    for(int r=0; r<rows_; ++r)
      for(int c=0; c<cols_; ++c)
	if (cell(r,c))
	  paint_cell(r,c,true);
  }

  void iterate() {
    char* src = usingBoard1_ ? board1_ : board2_;
    char* dest = usingBoard1_ ? board2_ : board1_;

    for(int r=0; r<rows_; ++r)
      for(int c=0; c<cols_; ++c) {
	int neighbors = 0;
	if (cell(src,r-1,c-1)) ++neighbors;
	if (cell(src,r-1,c  )) ++neighbors;
	if (cell(src,r-1,c+1)) ++neighbors;
	if (cell(src,r  ,c-1)) ++neighbors;
	if (cell(src,r  ,c+1)) ++neighbors;
	if (cell(src,r+1,c-1)) ++neighbors;
	if (cell(src,r+1,c  )) ++neighbors;
	if (cell(src,r+1,c+1)) ++neighbors;

	char val = neighbors == 3 || (neighbors == 2 && cell(src,r,c));
	if (immediate_update && val != cell(dest,r,c))
	  paint_cell(r,c,val);
	cell(dest,r,c) = val;
      }

    usingBoard1_ = !usingBoard1_;

    ++generations_;
    if ( (generations_ & 0xff) == 0) {
      time_t now;
      time(&now);
      cerr << generations_/(double)(now-start_) << " generations/sec\n";
    }
  }

  void redraw() {
    stop();

    if (immediate_update)
      draw_.set_drawable(screen(), buffer());
    else
      draw_.set_drawable(buffer());

    draw_.gc().set_foreground(Gdk_Color("black"));
    draw_.draw_rectangle(true,0,0,width(),height());

    init_board(rows_,cols_);
    randomize_board(-1);
    paint_board();

    start();
  }

  void action() {
    iterate();
    if (!immediate_update) {
      paint_board();
      refresh();
    }
  }


private:
  int rows_, cols_, NC, NR, offx_, offy_;
  char* board1_;
  char* board2_;
  bool usingBoard1_;
  int generations_;
  time_t start_;

  bool lastLive_;
  Gdk_Color live_, dead_;
  Gdk_Draw draw_;
  bool immediate_update;
};

main(int argc, char* argv[])
{
  Gtk_Main gtkmain(&argc, &argv);

  Gtk_Window main_window;
  Life life(100,100);

  main_window.add( &life );
  life.show();
  main_window.show();

  gtkmain.run();
}



// $Id$

// This is -*- C++ -*-
// $Id$

/*
 * starfield.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#include <stdlib.h>
#include <math.h>
#include "drawingbuffer.h"
#include <gdk--.h>
#include "timeout.h"

class Gtk_Stars : public Gtk_DrawingBuffer, public Gtk_Timeout {
public:
  Gtk_Stars() : Gtk_DrawingBuffer(), Gtk_Timeout() { 
    connect_to_method(button_press_event, this, &quit);
  }

  void redraw();
  void action();

  gint quit(GdkEventButton*) {
    stop();
    Gtk_Main::instance()->quit();
    return 0;
  }

private:
  Gdk_Draw draw_;
  const gint N = 200;
  const gint Ncomets = 20;
  int rotation;
  gdouble x[N], y[N], dx[N], dy[N];
  GdkPoint pts[N-Ncomets];
  GdkSegment seg[Ncomets];
};

void
Gtk_Stars::redraw()
{
  stop();

  rotation = 1;

  // We set up our drawable to paint everything twice... once directly
  // onto the window, and once onto the pixmap that we are using for
  // backing store.
  draw_.set_drawable(screen(), buffer());

  draw_.gc().set_function(GDK_COPY);
  draw_.gc().set_foreground(Gdk_Color("black"));
  draw_.draw_rectangle(true,0,0,width(),height());

  draw_.gc().set_foreground(Gdk_Color("white"));
  draw_.gc().set_function(GDK_XOR);

  draw_.gc().set_font(Gdk_Font("8x13bold"));

  start(30);

  // Initialize our star field
  for(gint i=0; i<N; ++i) {
    x[i] = width()/2;
    y[i] = height()/2;

    double r = ((width()+height())/80.0) * (0.8 + 0.4*random()/RAND_MAX);
    double th = 2*M_PI*random()/(double)RAND_MAX;
    dx[i] = r*cos(th);
    dy[i] = r*sin(th);

    int j = (random() % 20)+1;
    x[i] += j*dx[i];
    y[i] += j*dy[i];

    if (i < Ncomets) {
      seg[i].x1 = (gint)x[i];
      seg[i].y1 = (gint)y[i];
      seg[i].x2 = (gint)(x[i] + dx[i]);
      seg[i].y2 = (gint)(y[i] + dy[i]);
    } else {
      pts[i-Ncomets].x = (gint)x[i];
      pts[i-Ncomets].y = (gint)y[i];
    }
  }

  draw_.draw_points(pts, N-Ncomets);
  draw_.draw_segments(seg, Ncomets);
}

void
Gtk_Stars::action()
{
  draw_.draw_points(pts, N-Ncomets);
  draw_.draw_segments(seg, Ncomets);

  // change our rotation occasionally
  if (random() % 100 == 0) {
    rotation += random() % 3 - 1;
    if (rotation > 2) rotation = 1;
    if (rotation < -2) rotation = -1;
  }

  double cth = cos(rotation*2*M_PI/256);
  double sth = sin(rotation*2*M_PI/256);

  const gint w = width();
  const gint h = height();

  // increment our star field
  for(gint i=0; i<N; ++i) {
    x[i] += dx[i];
    y[i] += dy[i];

    // if we are rotating, handle it
    if (rotation) {
      double xx = cth*(x[i]-w/2) + sth*(y[i]-h/2) + w/2;
      double yy = -sth*(x[i]-w/2) + cth*(y[i]-h/2) + h/2;
      x[i] = xx;
      y[i] = yy;

      xx = cth*dx[i] + sth*dy[i];
      yy = -sth*dx[i] + cth*dy[i];
      dx[i] = xx;
      dy[i] = yy;
    }

    if (x[i] < 0 || x[i] > w || y[i] < 0 || y[i] > h) {
      x[i] = w/2 + dx[i];
      y[i] = h/2 + dy[i];
    }

    if (i < Ncomets) {
      seg[i].x1 = (gint)x[i];
      seg[i].y1 = (gint)y[i];
      seg[i].x2 = (gint)(x[i] + dx[i]);
      seg[i].y2 = (gint)(y[i] + dy[i]);
    } else {
      pts[i-Ncomets].x = (gint)x[i];
      pts[i-Ncomets].y = (gint)y[i];
    }

  }

  // draw_points (as well as draw_lines and draw_segments) are *big*
  // performance winners when compared to iterating over a loop and calling
  // draw_point N times.
  draw_.draw_points(pts, N-Ncomets);
  draw_.draw_segments(seg, Ncomets);
}

main(int argc, char* argv[])
{
  Gtk_Main gtkmain(&argc, &argv);

  Gtk_Window main_window;
  Gtk_Stars stars;

  main_window.add( &stars );
  stars.show();
  main_window.show();



  gtkmain.run();

}




// $Id$

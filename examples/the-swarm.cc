// This is -*- C++ -*-
// $Id$

/*
 * the-swarm.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#include <stdlib.h>
#include <math.h>
#include "drawingbuffer.h"
#include <gdk--.h>
#include "timeout.h"

class Gtk_Swarm : public Gtk_DrawingBuffer, public Gtk_Timeout {
public:
  Gtk_Swarm() : Gtk_DrawingBuffer(), Gtk_Timeout() {
    connect_to_method(button_press_event, this, &quit);
    connect_to_method(motion_notify_event, this, &motion);
    connect_to_method(enter_notify_event, this, &enter);
    connect_to_method(leave_notify_event, this, &leave);
  }

  void redraw();
  void action();
  void message();

  gint motion(GdkEventMotion*);
  gint enter(GdkEventCrossing*);
  gint leave(GdkEventCrossing*);



  gint quit(GdkEventButton*) {
    stop();
    Gtk_Main::instance()->quit();
    return 0;
  }

private:
  Gdk_Draw draw_;

  const gint N = 100;
  gint last_x, last_y;
  gdouble x[N], y[N], destx[N], desty[N], speed[N];
  GdkPoint pts[N];
};

void
Gtk_Swarm::redraw()
{
  stop();
  last_x = last_y = -1;

  draw_.set_drawable(screen(), buffer());

  draw_.gc().set_function(GDK_COPY);
  draw_.gc().set_foreground(Gdk_Color("black"));
  draw_.draw_rectangle(true,0,0,width(),height());

  draw_.gc().set_foreground(Gdk_Color("yellow"));
  draw_.gc().set_function(GDK_XOR);

  message();

  for(gint i=0; i<N; ++i) {
    x[i] = random() % width();
    y[i] = random() % height();
    destx[i] = random() % width();
    desty[i] = random() % height();
    speed[i] = 3 + (random() % 5);

    pts[i].x = (gint)x[i];
    pts[i].y = (gint)y[i];
  }

  draw_.draw_points(pts, N);

  start(15);
}

void
Gtk_Swarm::action()
{
  draw_.draw_points(pts, N);

  // Now all of the points walk towards their goal
  for(gint i=0; i<N; ++i) {
    gdouble dx = destx[i] - x[i];
    gdouble dy = desty[i] - y[i];
    if (dx*dx + dy*dy < speed[i]*speed[i]) {
      x[i] = destx[i];
      y[i] = desty[i];
      if (last_x == -1) {
	destx[i] = random() % width();
	desty[i] = random() % height();
      } else {
	double r = random() % 20;
	if (random() % 10 == 0) r *= 2;
	if (random() % 10 == 0) r *= 2;
	double th = 2*M_PI*random()/RAND_MAX;
	destx[i] = last_x + r*cos(th);
	desty[i] = last_y + r*sin(th);
      }
      speed[i] = 3 + (random() % 5);
    } else {
      gdouble len = sqrt(dx*dx + dy*dy);
      x[i] += dx * speed[i] / len;
      y[i] += dy * speed[i] / len;
    }

    pts[i].x = (gint)x[i];
    pts[i].y = (gint)y[i];
  }

  draw_.draw_points(pts, N);
}

void
Gtk_Swarm::message()
{
  draw_.gc_save();

  Gdk_Color c("white");
  draw_.gc().set_foreground(c);
  draw_.gc().set_function(GDK_XOR);
  draw_.draw_aligned_string(width()/2, height()/2, "Move Into The Box");
  
  draw_.gc_restore();
}

gint
Gtk_Swarm::motion(GdkEventMotion* em)
{
  last_x = (gint)em->x;
  last_y = (gint)em->y;
  return 0;
}

gint
Gtk_Swarm::enter(GdkEventCrossing*)
{
  message();
}

gint
Gtk_Swarm::leave(GdkEventCrossing*)
{
  message();
  last_x = last_y = -1;
}

main(int argc, char* argv[])
{
  Gtk_Main gtkmain(&argc, &argv);

  Gtk_Window main_window;
  Gtk_Swarm swarm;

  main_window.add( &swarm );
  swarm.show();
  main_window.show();


  gtkmain.run();
}





// $Id$

// This is -*- C++ -*-
// $Id$

/* 
 * drawingbuffer.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#include "drawingbuffer.h"

Gtk_DrawingBuffer::Gtk_DrawingBuffer() : Gtk_DrawingArea(),
  width_(0), height_(0), buffer_(0)
{
  // Make sure we get all of the events that Gtk_DrawingArea doesn't
  // usually see.
  gint mask = gtk_widget_get_events (GTK_WIDGET (gtkobject));
  mask |= GDK_EXPOSURE_MASK;
  mask |= GDK_POINTER_MOTION_MASK;
  //  mask |= GDK_POINTER_MOTION_HINT_MASK; // interferes with regular pointer motion events.
  mask |= GDK_BUTTON1_MOTION_MASK;
  mask |= GDK_BUTTON_PRESS_MASK;
  mask |= GDK_BUTTON_RELEASE_MASK;
  mask |= GDK_KEY_PRESS_MASK;
  mask |= GDK_ENTER_NOTIFY_MASK;
  mask |= GDK_LEAVE_NOTIFY_MASK;
  gtk_widget_set_events(GTK_WIDGET(gtkobject), mask);
}

Gtk_DrawingBuffer::~Gtk_DrawingBuffer()
{
  if (buffer_)
    gdk_pixmap_unref(buffer_);
}

gint
Gtk_DrawingBuffer::expose_event_impl(GdkEventExpose* e)
{
  refresh(e->area.x, e->area.y, e->area.width, e->area.height);
  return Gtk_DrawingArea::expose_event_impl(e);
}

gint
Gtk_DrawingBuffer::configure_event_impl(GdkEventConfigure* c)
{
  if ( (c->width != width_) || (c->height != height_) ) {
    new_pixmap(c);
    if (buffer_ != 0) redraw();
  }
  return Gtk_DrawingArea::configure_event_impl(c);
}

void 
Gtk_DrawingBuffer::new_pixmap(GdkEventConfigure * c)
{
  // Create a new pixmap if the size changes.
  if ( (c->width != width_) || (c->height != height_) ) {
    if (buffer_)
      gdk_pixmap_unref(buffer_);
    
    width_ = c->width;
    height_ = c->height;

    // Don't create a pixmap with a 0 dimension
    if (width_ > 0 && height_ > 0) {
      buffer_ = gdk_pixmap_new(GTK_WIDGET(gtkobject)->window,
			       width_,height_,
			       -1);
    }
    else buffer_ = 0;
  }
}

void
Gtk_DrawingBuffer::refresh(gint x, gint y, gint w, gint h)
{
  if (buffer_ != 0) {
    gdk_draw_pixmap(GTK_WIDGET(gtkobject)->window,
		    GTK_WIDGET(gtkobject)->style->fg_gc
		    [GTK_WIDGET_STATE (gtkobject)],
		    buffer_,
		    x,y,x,y,w,h);
  }
}




// $Id$




// This is -*- C++ -*-
// $Id$

/* 
 * drawingbuffer.h
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifndef _INC_DRAWINGBUFFER_H
#define _INC_DRAWINGBUFFER_H

#include <gdk/gdk.h>
#include <gtk--.h>

class Gtk_DrawingBuffer : public Gtk_DrawingArea {
public:
  Gtk_DrawingBuffer();
  virtual ~Gtk_DrawingBuffer();
  
  /*
    screen() lets us draw directly to the screen, while
    buffer() lets us draw directly to the backing store pixmap.
   */
  GdkDrawable* screen() { 
    g_return_val_if_fail(realized(),0);
    return (GdkDrawable*)GTK_WIDGET(gtkobject)->window; 
  }

  /* buffer() will occasionally return 0, e.g. if the widget has a 0 allocation. */
  GdkDrawable* buffer() { return (GdkDrawable*)buffer_; }

  gint width() const { return width_; }
  gint height() const { return height_; }

  /*
    The refresh() routines copy all or part of our backing pixmap
    to the screen.  Until it is called, things drawn on the buffer
    will not be visible.
  */
  virtual void refresh(gint x, gint y, gint w, gint h);
  void refresh() { refresh(0, 0, width(), height()); }

  virtual void redraw() { }

protected:
  
  virtual gint expose_event_impl(GdkEventExpose*);
  virtual gint configure_event_impl(GdkEventConfigure*);
  void new_pixmap(GdkEventConfigure * c);

private:
  gint width_, height_;
  GdkPixmap* buffer_;
};

#endif // _INC_DRAWINGBUFFER_H

// $Id$

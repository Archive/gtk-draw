// -*- C++ -*-

/* 
 * gtkwrapper.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GUPPI_GTKWRAPPER_H
#define GUPPI_GTKWRAPPER_H

#include <gtk--.h>

// This conceals the real nature of a widget while letting you call
// widget functions on it. Permits the Foo/FooImpl trick I like to use.

// In the future this will probably derive from Gtk_Bin; in general
//  you should pretend it's a Gtk_Widget and ignore the more specific
//  functions.

class Gtk_Wrapper : public Gtk_Frame {
public:
  Gtk_Wrapper() :  Gtk_Frame(), impl_(0) {}
  Gtk_Wrapper(Gtk_Widget* impl) : Gtk_Frame(), 
    impl_(impl) { add(impl_); }
  virtual ~Gtk_Wrapper() { 
    if (impl_) {
      impl_->hide();
      delete impl_;
    }    
  }

protected: 
  void set_impl(Gtk_Widget* impl) {
    g_return_if_fail(impl_ == 0);
    impl_ = impl;
    add(impl_);
  }

private:
  Gtk_Widget* impl_;
};

#endif


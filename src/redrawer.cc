// -*- C++ -*-

/* 
 * redrawer.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "redrawer.h"

Redrawer::~Redrawer()
{
  // prevents redrawing a destroyed buffer, assuming the
  //  buffer properly destroys the redrawer.
  if (idle_connected_) {
    c.disconnect();
  }
}

void 
Redrawer::queue()
{
  if (!idle_connected_) {
    c = connect_to_method(Gtk_Main::idle(), 
			  this,
			  &Redrawer::redrawidle);
    idle_connected_ = true;
  }
}

gint
Redrawer::redrawidle()
{
  g_assert(idle_connected_);
  
  gdb_.redraw();

  idle_connected_ = false;
  return 0; // causes disconnect
}


// -*- C++ -*-

/* 
 * redrawer.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef _INC_REDRAWER_H
#define _INC_REDRAWER_H

#include "drawingbuffer.h"

// This class is used to add functionality to a Gtk_DrawingBuffer.
// It's useful to prevent multiple redraws; it will only queue a 
// single redraw, which is executed as soon as we're idle.

class Redrawer : public Gtk_Signal_Base {
public:
  Redrawer(Gtk_DrawingBuffer & gdb) : gdb_(gdb), idle_connected_(false) {}
  ~Redrawer();
  
  void queue();
  
private:
  Gtk_DrawingBuffer & gdb_;

  gint redrawidle();  
  
  bool idle_connected_;

  Connection c;

  Redrawer();
};


#endif

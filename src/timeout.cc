// This is -*- C++ -*-
// $Id$

/* 
 * timeout.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#include "timeout.h"

gint
gtkmm_timeout_function_hack(gpointer p)
{
  Gtk_Timeout* timeout = (Gtk_Timeout*)p;
  timeout->action();
  return 1;
}

void
Gtk_Timeout::start(guint32 i)
{
  if (running()) stop();
  timeout_interval = i;
  timeout_tag = gtk_timeout_add(timeout_interval,
				gtkmm_timeout_function_hack, this);
}

void
Gtk_Timeout::stop()
{
  if (running()) {
    gtk_timeout_remove(timeout_tag);
    timeout_tag = -1;
  }
}

///////////////////////////////////////////////////////////////////////

gint
gtkmm_idle_function_hack(gpointer p)
{
  Gtk_Idle* idle = (Gtk_Idle*)p;
  idle->action();
  return 1;
}


void
Gtk_Idle::start()
{
  if (running()) stop();
  idle_tag = gtk_idle_add(gtkmm_idle_function_hack, this);
}

void
Gtk_Idle::stop()
{
  if (running()) {
    gtk_idle_remove(idle_tag);
    idle_tag = -1;
  }
}


// $Id$

// This is -*- C++ -*-
// $Id$

/* 
 * timeout.h
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */


#ifndef _INC_TIMEOUT_H
#define _INC_TIMEOUT_H

#include <gtk--.h>

class Gtk_Timeout {
public:
  Gtk_Timeout() : timeout_interval(0), timeout_tag(-1) { }

  void start(guint32 interval_in_milliseconds);
  void stop();

  guint32 interval() const { return timeout_interval; }
  bool running() const { return timeout_tag != -1; }

  virtual void action() = 0;
  
private:
  guint32 timeout_interval;
  gint timeout_tag;
};

class Gtk_Idle {
public:
  Gtk_Idle() : idle_tag(-1) { }

  void start();
  void stop();

  bool running() const { return idle_tag != -1; }

  virtual void action() = 0;

private:
  gint idle_tag;
};

#endif // _INC_TIMEOUT_H

// $Id$
